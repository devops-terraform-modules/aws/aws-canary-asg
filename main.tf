locals {
  resources = {
    name                          = var.resources.name
    vpc_id                        = var.resources.vpc_id
    asg_min_size                  = var.resources.asg_min_size
    asg_max_size                  = var.resources.asg_max_size
    asg_desired_capacity          = var.resources.asg_desired_capacity
    blue_user_data_file           = try(var.resources.blue_user_data_file, null)
    green_user_data_file          = try(var.resources.green_user_data_file, null)
    instances_security_groups_ids = var.resources.instances_security_groups_ids
    alb_security_groups_ids       = var.resources.alb_security_groups_ids
    alb_subnets_ids               = toset(var.resources.alb_subnets_ids)
    traffic_distribution          = var.resources.traffic_distribution
    blue_ami                      = var.resources.blue_ami
    green_ami                     = var.resources.green_ami
    blue_instance_type            = var.resources.blue_instance_type
    green_instance_type           = var.resources.green_instance_type
    tags                          = try(var.resources.tags != null ? var.resources.tags : null, null)
  }

  traffic_dist_map = {
    blue = {
      blue  = 100
      green = 0
    }
    blue-90 = {
      blue  = 90
      green = 10
    }
    split = {
      blue  = 50
      green = 50
    }
    green-90 = {
      blue  = 10
      green = 90
    }
    green = {
      blue  = 0
      green = 100
    }
  }


  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_lb" "alb" {
  desync_mitigation_mode           = "defensive"
  drop_invalid_header_fields       = false
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = true
  enable_http2                     = true
  enable_waf_fail_open             = false
  idle_timeout                     = 60
  internal                         = false
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  security_groups                  = local.resources.alb_security_groups_ids
  subnets                          = local.resources.alb_subnets_ids
}

resource "aws_alb_listener" "default" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "forward"
    forward {
      target_group {
        arn    = aws_alb_target_group.blue.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "blue", 100)
      }

      target_group {
        arn    = aws_alb_target_group.green.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "green", 0)
      }

      stickiness {
        enabled  = true
        duration = 60
      }
    }
  }

  depends_on = [aws_lb.alb]
}

resource "aws_alb_target_group" "blue" {
  connection_termination             = false
  deregistration_delay               = "300"
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }
}

resource "aws_alb_target_group" "green" {
  connection_termination             = false
  deregistration_delay               = "300"
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }
}


resource "aws_launch_template" "blue" {
  name_prefix            = "launch_blue-"
  image_id               = local.resources.blue_ami
  instance_type          = local.resources.blue_instance_type
  vpc_security_group_ids = local.resources.instances_security_groups_ids
  user_data              = local.resources.blue_user_data_file != null ? base64encode(templatefile(local.resources.blue_user_data_file, {})) : null

  lifecycle {
    create_before_destroy = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
      Name = "launch_blue - ALB ${local.resources.name}"
    })
  }
}
resource "aws_autoscaling_group" "asg_blue" {
  target_group_arns   = toset([aws_alb_target_group.blue.arn])
  desired_capacity    = local.resources.asg_desired_capacity
  min_size            = local.resources.asg_min_size
  max_size            = local.resources.asg_max_size
  vpc_zone_identifier = local.resources.alb_subnets_ids

  launch_template {
    id      = aws_launch_template.blue.id
    version = "$Latest"
  }

  depends_on = [aws_launch_template.blue]
}

resource "aws_launch_template" "launch_green" {
  name_prefix            = "launch_green-"
  image_id               = local.resources.green_ami
  instance_type          = local.resources.green_instance_type
  vpc_security_group_ids = local.resources.instances_security_groups_ids
  user_data              = local.resources.green_user_data_file != null ? base64encode(templatefile(local.resources.green_user_data_file, {})) : null

  lifecycle {
    create_before_destroy = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
      Name = "launch_green - ALB ${local.resources.name}"
    })
  }
}
resource "aws_autoscaling_group" "asg_green" {
  target_group_arns   = toset([aws_alb_target_group.green.arn])
  vpc_zone_identifier = local.resources.alb_subnets_ids
  desired_capacity    = local.resources.asg_desired_capacity
  min_size            = local.resources.asg_min_size
  max_size            = local.resources.asg_max_size

  launch_template {
    id      = aws_launch_template.launch_green.id
    version = "$Latest"
  }

  depends_on = [aws_launch_template.launch_green]
}
