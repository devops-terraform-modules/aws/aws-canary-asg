output "alb_endpoint" {
  value = module.module_usage_howto.alb_endpoint
}

output "debug" {
  value = module.module_usage_howto.debug
}
