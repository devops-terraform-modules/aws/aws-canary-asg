provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = {
    name                          = "basic_canary"
    asg_desired_capacity          = 2
    asg_min_size                  = 1
    asg_max_size                  = 2
    blue_ami                      = "ami-xxxxx"
    green_ami                     = "ami-xxxxx"
    blue_user_data_file           = "${path.cwd}/scripts/blue_user_data.sh"
    green_user_data_file          = "${path.cwd}/scripts/green_user_data.sh"
    blue_instance_type            = "t3.micro"
    green_instance_type           = "t3.micro"
    traffic_distribution          = "split"
    vpc_id                        = "vpc-xxxxx"
    instances_security_groups_ids = toset(["sg-xxxxx"])
    alb_security_groups_ids       = toset(["sg-xxxxx"])
    alb_subnets_ids               = toset(["subnet-xxxxx", "subnet-xxxxx", "subnet-xxxxx"])
    tags = {
      Name = "basic-canary-asg"
    }
  }
}
